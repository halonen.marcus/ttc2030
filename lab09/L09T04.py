# Avataan tiedosto
with open("nimet.txt", "r") as nimet:

# Luodaan Dictionary
    dict = {}

# Lasketaan toistuvat nimet
    for nimi in nimet:
        if nimi not in dict:
            dict[nimi] = 0
        dict[nimi] += 1

# Tulostetaan dictionary
    print(dict)
    print(f"Total number of words: {len(dict)}")