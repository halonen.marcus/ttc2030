import random

# Kysytaan rivien maara
x = int(input("Anna lottorivien määrä: "))

# Kaytetaan randomia saamaan 7 uniikkia lukua
def lotto():
    return str(random.sample(range(1, 40), 7))

# Kirjoitetaan lottonumerot tiedostoon
with open("lotto.txt", "w") as a:
    a.write(lotto() * x)