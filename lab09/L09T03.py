# Luodaan lista nrolle
nrot = []

# Kysytaan nroa
while True:
    nro = input("Anna kokonaisluku: ")
    if len(nro) == 0:
        break
    nrot.append(nro)

# Kirjoitetaan numerot tiedostoon
with open("L09T03MH.txt", "w") as a:
    for b in nrot:
        a.write(b + " ")

# Lisataan tiedoston loppuun uudelle riville lukujen yhteismaara
with open("L09T03MH.txt", "a") as a:
    a.write(f"\nSyötetty: {len(nrot)} lukua")