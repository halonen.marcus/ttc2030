try:
# Avataan tiedosto tallennusta varten
    file = open("L09T01MH.txt", "w")

# Luodaan lista nimille
    nimet = []

# Kysytaan nimia
    while True:
        nimi = input("Anna sukunimi: ")
        if len(nimi) == 0:
            break
        nimet.append(nimi)

# Kirjoitetaan nimet tiedostoon
    for name in nimet:
        file.write(name + ' ')
    file.close()
except PermissionError:
    print("No permission")
except:
    print("Error")