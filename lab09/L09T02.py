# Tulostetaan tiedoston sisalto
with open("L09T01MH.txt", "r") as nimet:
    print(nimet.read())

# Tulostetaan tiedoston sisalto vaarinpain
with open("L09T01MH.txt", "r") as nimet:
    print(*sorted(nimet.read().split()))