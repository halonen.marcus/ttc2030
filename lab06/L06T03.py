# Luodaan lista
lista = []

# Kysytään nimet
while True:
    nimi = input("Anna oppilaan nimi: ")
    if not nimi:
        break
    lista.append(nimi)

# Annetaan parametrit ja tulostetaan tulos
print("Oppilaita:", len(lista))
print(*lista, sep = ", ")