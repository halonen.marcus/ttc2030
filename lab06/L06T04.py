# Luodaan lista
lista = []

# Kysytään pisteet
for i in range(5):
    lista.append(int(input("Hypyn pisteet: ")))

# Poistetaan isoin ja pienin luku
lista.remove(max(lista))
lista.remove(min(lista))

# Tulostus
print("Pisteet:", sum(lista))