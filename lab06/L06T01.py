# Luodaan showtime() funktio
def showtime(secs):
    hours = secs // 3600
    minutes = secs // 60 % 60
    seconds = secs % 60
    return f"{hours:0>2}:{minutes:0>2}:{seconds:0>2}"

# Annetaan parametrit ja tulostetaan tulos
print(showtime(500))
print(showtime(10000))
print(showtime(121000))