# Luodaan muuttuja(t)
tuuma = 2.54

# Luodaan funktio
def show_cm(x):
    return (f"{x} tuumaa on {x * tuuma} cm")

# Annetaan parametri ja tulostetaan tulos
print(show_cm(2))
print(show_cm(4.5))
print(show_cm(12))