# Luodaan funktiot, laskutoimituksilla muunnokset
def celToFah(x):
    return (x * 1.8) + 32

def fahToCel(x):
    return (x - 32) * .5556

# Annetaan parametrit ja tulostetaan tulos
print(f"{celToFah(10.0):.1f}")
print("{:.1f}".format(fahToCel(38.0)))