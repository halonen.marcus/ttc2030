# Kysytään käyttäjän nimi
nimi = input("Anna nimesi: ")
first, last = nimi.split()

# Tulostetaan etunimi ja sukunimi erikseen
print("Etunimesi:", first)
print("Sukunimesi:", last)