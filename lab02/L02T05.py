# Kysytään käyttäjän nimi
nimi = input("Anna etunimesi: ")
lkm = len(nimi)

# Tulostetaan nimi ja pituus
print("Nimessäsi", nimi, "on", lkm, "kirjainta.")

# Tulostetaan nimen ensimmäistä kirjainta yhtä paljon kuin sanassa on kirjaimia
print(nimi.upper()[0] * lkm)