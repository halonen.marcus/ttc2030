# Kysytään käyttäjän nimi ja syntymävuosi
nimi = input("Anna etunimesi: ")
birth = input("Syntymävuotesi: ")
vuosi = 2021

# Lasketaan ikä
ika = vuosi - int(birth)

# Tulostetaan nimi ja ikä
print("Hei", nimi.capitalize() + ",", "olet", ika, "vuotta.")