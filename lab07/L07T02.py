# Luodaan luokka ja käytetään f-stringiä helpottamaan tulostusta
class Human:
    name = ""
    age = ""
    def __str__(self):
        return (f"Nimi: {self.name}, ikä: {self.age}")

# Luodaan kaksi eri oliota
human1 = Human()
human1.name = "Adam"
human1.age = "19"

human2 = Human()
human2.name = "Eva"
human2.age = "18"

# Tulostetaan olioiden tiedot
print(human1)
print(human2)