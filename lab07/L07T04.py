# Luodaan luokka
class Mobile:
    brand = ""
    model = ""
    price = ""
    def __str__(self):
        return (f"Brand: {self.brand}, Model: {self.model}, Price: {self.price}")
    def __init__(self, brand = "", model = "", price = ""):
        self.brand = brand
        self.model = model
        self.price = price

# Luodaan kaksi eri objektia eri tiedoilla
phone1 = Mobile("Samsung", "Galaxy", 349)
phone2 = Mobile("Apple", "iPhone 12", 899)

# Tulostetaan olioiden tiedot
print(phone1)
print(phone2)