# Luodaan luokka
class Car:
    brand = ""
    model = ""
    price = ""
    def __str__(self):
        return (f"auto: {self.brand} {self.model} {self.price}")
    def __init__(self, brand = "", model = "", price = ""):
        self.brand = brand
        self.model = model
        self.price = price

# Luodaan kolme eri objektia eri tiedoilla
car1 = Car("Skoda", "Octavia", 3000)
car2 = Car("Audi", "A4", 4000)
car3 = Car("Volvo", "V70", 5000)

# Lisätään hinnat muuttujaan
cars = car1.price, car2.price, car3.price

# Tulostetaan olioiden tiedot ja summat
print(car1)
print(car2)
print(car3)
print(f"Autojen hinta yhteensä: {sum(cars)}")