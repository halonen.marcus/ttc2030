# Luodaan luokka
class Cat:
    name = ""
    color = ""
    def __str__(self):
        return f"{self.name}, color: {self.color}"
    def miau(self):
        return f"{self.name} says: Meoooooow!"

# Luodaan kaksi eri oliota
cat1 = Cat()
cat1.name = "Kit"
cat1.color = "black"

cat2 = Cat()
cat2.name = "Kat"
cat2.color = "white"

# Tulostetaan olioiden tiedot ja laitetaan kissat maukumaan
print(cat1)
print(cat2)
print(cat1.miau())
print(cat2.miau())