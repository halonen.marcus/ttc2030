# Luodaan luokka
class Pelikortti:
    maa = ""
    arvo = ""

# Luodaan viisi eri oliota eri tiedoilla
kortti1 = Pelikortti()
kortti1.maa = "pata"
kortti1.arvo = "2"

kortti2 = Pelikortti()
kortti2.maa = "hertta"
kortti2.arvo = "3"

kortti3 = Pelikortti()
kortti3.maa = "risti"
kortti3.arvo = "4"

kortti4 = Pelikortti()
kortti4.maa = "ruutu"
kortti4.arvo = "5"

kortti5 = Pelikortti()
kortti5.maa = "pata"
kortti5.arvo = "6"

# Tulostetaan olioiden tiedot
print(kortti1.maa.capitalize(), kortti1.arvo)
print(kortti2.maa.capitalize(), kortti2.arvo)
print(kortti3.maa.capitalize(), kortti3.arvo)
print(kortti4.maa.capitalize(), kortti4.arvo)
print(kortti5.maa.capitalize(), kortti5.arvo)