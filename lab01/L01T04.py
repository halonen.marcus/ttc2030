# Kysytään käyttäjältä kaksi kokonaislukua
ekaLuku = input("Anna kokonaisluku: ")
tokaLuku = input("Anna toinen kokonaisluku: ")

# Tulostetaan laskutoimitukset
print("Summa:", int(ekaLuku) + int(tokaLuku))
print("Erotus:", int(ekaLuku) - int(tokaLuku))
print("Tulo:", int(ekaLuku) * int(tokaLuku))