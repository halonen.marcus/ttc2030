# Kysytään käyttäjältä kaksi kokonaislukua
ekaLuku = input("Anna kokonaisluku: ")
tokaLuku = input("Anna toinen kokonaisluku: ")

# Tulostetaan yhdyslaskun tulos
print("Summa:", int(ekaLuku) + int(tokaLuku))