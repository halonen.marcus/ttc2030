import random

# Luodaan funktio
def lotto():
    return random.sample(range(1, 40), 7)

# Tulostetaan funktio, numerot erotettuna pilkuilla
print(*lotto(), sep = ',')