# Luodaan lista
arvosanat = []

# Kysytään arvosanoja
while True:
    a = input("Kurssin arvosana: ")
    if len(a) == 0:
        break
    inta = int(a)
    if inta >= 6:
        break
    arvosanat.append(inta)

# Lasketaan keskiarvo
keskiarvo = sum(arvosanat) / len(arvosanat)

# Tulostetaan aakkosjärjestyksessä
print(f"Arvosanojen kokonaismäärä: {len(arvosanat)}")

# Tulostetaan keskiarvo
print(f"Arvosanojen keskiarvo: {keskiarvo}")