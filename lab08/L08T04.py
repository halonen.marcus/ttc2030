# Luodaan dictionary
autot = {"BBB-111":"Acura","AAA-222":"Chevrolet","CCC-333":"Audi","DDD-444":"BMW","EEE-555":"Buick",
"FFF-666":"Bentley","GGG-777":"Cadillac","HHH-888":"Ford","III-999":"Dodge","JJJ-000":"Fiat"}

# Tulostetaan autot järjestyksessä rekisterinumeron mukaan ja näytetään autonmerkki samalla
print(sorted(autot.items()))

# Alla veljeni opettama tapa tehdä tämä tehtävä
def format(a):
    return f"{a[0]}:{a[1]}"

print('\n'.join(map(format,sorted(autot.items()))))