# Luodaan lista
rekkari = []

# Kysytään rekisterejä
while True:
    rek = input("Anna rekisterinumero: ")
    if rek == 0:
        break
    rekkari.append(rek)

# Tulostetaan aakkosjärjestyksessä
print(*sorted(rekkari))