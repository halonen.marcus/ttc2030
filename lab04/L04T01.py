# Kysytään luku
luku = int(input("Montako lukua: "))

# Tulostetaan haluttu määrä lukuja kertaa kymmenen
for x in range(luku):
    print(str(x + 1) + ".luku", x * 10)