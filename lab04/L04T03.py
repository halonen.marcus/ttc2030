# Luodaan lista

lista = []

# Kysytään lukuja
while True:
    luku = input("Anna luku: ")
    if not luku:
        break
    lista.append(int(luku))

# Tulostetaan listan lukumäärä ja summa
print(len(lista))
print(sum(lista))