# Kysytään luku
luku = int(input("Anna numero väliltä 1-10: "))

# Näytetään luku ykkösestä lähtien käyttäjän antamaan lukuun ja lasketaan niiden neliö
for x in range (luku):
    print("Luvun", x + 1, "neliö on", (x + 1) ** 2)