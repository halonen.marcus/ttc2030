# Kysytään luvut
kluku = int(input("Anna ensimmäinen kokonaisluku: "))
tkluku = int(input("Anna toinen kokonaisluku: "))
kkluku = int(input("Anna kolmas kokonaisluku: "))

# Lisätään luvut listaan
luvut = [kluku, tkluku, kkluku]

# Tulostetaan suurin listassa oleva luku:
print("Suurin:", max(luvut))