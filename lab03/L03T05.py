# Kysytään luvut
aluku = int(input("Anna 1.luku: "))
bluku = int(input("Anna 2.luku: "))
cluku = int(input("Anna 3.luku: "))
dluku = int(input("Anna 4.luku: "))
eluku = int(input("Anna 5.luku: "))

# Lisätään luvut listaan
luvut = [aluku, bluku, cluku, dluku, eluku]

# Yhdella rivilla
# luvut = [int(input(f"Anna {n}.luku: ")) for n in range(1,6)]

# Source: https://www.geeksforgeeks.org/python-remove-negative-elements-in-list/
aluvut = [a for a in luvut if a >= 0]

print(sum(aluvut))