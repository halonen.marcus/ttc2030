# Kysytään luku
luku = int(input("Pisteet: "))

# Käydään luvut läpi
if (luku >= 0 and luku <= 1):
    print("Arvosana: 0")
elif (luku >= 2 and luku <= 3):
    print("Arvosana: 1")
elif (luku >= 4 and luku <= 5):
    print("Arvosana: 2")
elif (luku >= 6 and luku <= 7):
    print("Arvosana: 3")
elif (luku >= 8 and luku <= 9):
    print("Arvosana: 4")
elif (luku >= 10 and luku <= 12):
    print("Arvosana: 5")