# Luodaan funktio
def average(x, z, y):
    return (x + z + y) / 3

# "{:.1f}".format koodinpätkän löysin: https://stackoverflow.com/questions/3400965/getting-only-1-decimal-place
# Tulostetaan laskutoimituksen tulos
print("{:.1f}".format(average(2,4,6)))
print("{:.1f}".format(average(5,5,6)))