# Luodaan funktio ja suoritetaan laskutoimitus
def get_fuel(x, z):
    return x / (100 / z) 

# "{:.1f}".format koodinpätkän löysin: https://stackoverflow.com/questions/3400965/getting-only-1-decimal-place
# Tulostetaan matkan kulutusmäärä
print("{:.1f}".format(get_fuel(100,7.5)))
print("{:.1f}".format(get_fuel(220,6.9)))