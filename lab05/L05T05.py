# Luodaan funktio ja suoritetaan laskutoimitus
def get_cost(a, b, c):
    return a / (100 / b) * c 
print(type(get_cost))
# Annetaan parametrit ja tulostetaan funktion get_cost vastaus
print("{:.2f}".format(get_cost(100,7.5,1.88)), "€")
print("{:.2f}".format(get_cost(220,6.9,1.88)), "€")