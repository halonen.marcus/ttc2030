# Kysytaan ika
ika = int(input("What's your age? "))

# Luodaan funktio
def kerro3():
    if ika < 0:
        return "You cant be less than 0 years old"
    elif ika < 13:
        return "child"
    elif ika <= 19:
        return "teen"
    elif ika >= 20 and ika <= 65:
        return "adult"
    else:
        return "senior"

print(kerro3())