import calendar

# Luodaan funktio ja kaytetaan ehtolauseita jotta voidaan
# tulostaa vastaus tehtavanannon vaatimalla tavalla
def leap(x):
    if calendar.isleap(x) == True:
        return f"{x} is a leap year!"
    else:
        return f"{x} is not a leap year!"

print(leap(int(input("Insert year: "))))