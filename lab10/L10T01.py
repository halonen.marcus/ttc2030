# Luodaan saldo muuttuja
saldo = 2000

# Tulostetaan saldon maara
print(f"Bank account balance: {saldo} €")

# Kysytaan montako euroa ja senttia lisataan saldoon
euro = int(input("How many euros will be added to the balance? "))
sentti = int(input("How many cents will be added to the balance? ")) * 0.01
summa = euro + sentti

# Tulostetaan uusi saldo
print(f"Bank account balance: {saldo + summa} €")