# Luodaan lista jossa on neljä elementtiä
lista = [1,2,3,4]

# Kokeillaan muuttaa viidennettä elementtiä nollaksi
try:
    lista[4] = 0
# Error tarkoittaa tässä tilanteessa ettei listassa ole tarpeeksi elementtejä
except IndexError:
    print("Listassa ei ole tarpeeksi elementtejä")